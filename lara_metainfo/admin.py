"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo admin *

:details: lara_metainfo admin module admin backend configuration.
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20180625
:date: (last modification) 20190125

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.contrib import admin

from .models import ItemClass, MetaInfo

class ItemClassAdmin(admin.ModelAdmin):
    list_display = ('item_class','description')
    list_filter = ['item_class']
    search_fields = ['item_class']
    
class MetaInfoAdmin(admin.ModelAdmin):
    list_display = ('metainfo_id','description')
    list_filter = ['metainfo_class']
    search_fields = ['metainfo_class']
            
admin.site.register(ItemClass, ItemClassAdmin)
admin.site.register(MetaInfo, MetaInfoAdmin)
