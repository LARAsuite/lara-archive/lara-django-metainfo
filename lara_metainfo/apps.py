"""_____________________________________________________________________

:PROJECT: LARA

*lara_metainfo apps *

:details: lara_metainfo app configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.1/ref/applications/

:file:    apps.py
:authors: mark doerr

:date: (creation)          20180624
:date: (last modification) 20180625

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.apps import AppConfig

class LaraMetainfoConfig(AppConfig):
    name = 'lara_metainfo'
    #~ verbose_name = "enter a verbose name for your app: lara_metainfo here - this will be used in the admin interface"
    #~ lara_app_icon = 'lara_metainfo_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

